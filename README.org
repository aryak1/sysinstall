#+title: SYSINSTALL
* What is sysinstall
  Sysinstall is a frontend for various package managers

  How many times have you done apt-get on arch, pacman on dnf etc. etc.

  Sysinstall aims to unify them into one script and intelligently chose the correct package manager for your distro.
* How to install it
  The only requirements are
  - a posix compatible shell symlinked to /bin/sh
  - a package manager supported by sysinstall (see 3.0 for more info on that)

  You can install the package with the Makefile provided
  #+begin_src sh
    git clone https://github.com/gi-yt/sysinstall
    cd sysinstall
    sudo make install
  #+end_src
* Supported Package Managers
  sysinstall currently supports
  - APT (Debian/Ubuntu)
  - PORTAGE (Gentoo)
  - XBPS (Void Linux)
  - DNF (Fedora/RHEL 8)
  - ZYPPER (OpenSUSE)
* Limitations and future plans
** NIX / GUIX
   For implementing these i need to work on multiple package manager support
** Custom UI Frontend
   This is hard and is gonna be the last thing I will probably add
** Config file
   This is aldready planned and will be worked on
** BSD Support
   Very Very Soon
  

* Codeberg
  This project is available in [[https://codeberg.org/aryak/sysinstall][codeberg]]
